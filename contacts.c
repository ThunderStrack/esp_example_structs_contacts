//-----------------------------------------------------------------------------
// contacts.c
//
// Small example program for the use of structs and linked lists in c
// The main purpose of this program is not functionality but rather
// a small overview of how structs can be used in c.
//
//
// Group: 8 study assistant Martin Winter
//
// Authors: Lukas Stracke 1610738
//
// Latest Changes: 27.11.2016 (by Lukas Stracke)
//-----------------------------------------------------------------------------
//

//Header files

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//Constants
#define SUCCESS 0
#define ERR_CODE_OOM 1
#define ERR_MSG_OOM "Out of Memory!\n"
#define BUFFER_SIZE 20


//Booleans make your life easier
typedef enum
{
  false,
  true
} Boolean;

//Our main struct "Contact"
typedef struct _Contact_
{
  int id_;
  char *name_;
  unsigned short age_;
  char *email_address_;
  char *tel_number_;
  struct _Contact_ *next_;
  struct _Contact_ *prev_;
} Contact;

//function prototypes:
void printStartScreen();

void printMenu();

Contact enterContact();

char *getInput();

void enterNewContact(Contact *all_contacts);

int getLengthOfAllContacts(Contact *all_contacts);

void freeAll(Contact *all_contacts);

/**-----------------------------------------------------------------------------
 * main()
 * main function of the program
 *
 * @author Stracke, 27.11.2016
 * @return 0 on success, other value on errors (see definition of error codes)
 */
int main()
{
  printStartScreen();

  // create new dynamic array of contacts (size 1 at the beginning)
  Contact *all_contacts = (Contact *) malloc(sizeof(Contact));

  //User has to enter a starting contact at the beginning
  all_contacts[0] = enterContact();
  all_contacts[0].next_ = NULL;
  all_contacts[0].prev_ = NULL;

  printf("\nName: %s", all_contacts[0].name_);
  printf("\nMail: %s", all_contacts[0].email_address_);
  printf("\nTele: %s\n", all_contacts[0].tel_number_);

  printMenu();

  unsigned short action_flag;

  //printf("\nAge: ");
  scanf("%hu", &action_flag);

  printf("\nDEBUG: Action: %d", action_flag);

  switch (action_flag)
  {
    case 1 : //show all contacts
    {
      //TODO: show Contacts
      break;
    }
    case 2 : //enter new contact
    {
      printf("\nDEBUG: new: %d", action_flag);
      enterNewContact(all_contacts);
      break;
    }
    case 3 : //exit
    {
      freeAll(all_contacts);
      exit(SUCCESS);
    }
    default:
    {
      break;
    }
  }

  free(all_contacts);

  return 0;
}

/**
 * enterNewContact
 * adds new item to all_contacts array
 * @param all_contacts (by reference)
 */
void enterNewContact(Contact *all_contacts)
{

  int length = getLengthOfAllContacts(all_contacts);
  printf("\nDEBUG: length of all_contacts before realloc: %d", length);


  printf("\nDEBUG: starting newCOntact");
  //first we try to allocate memory for another Contact
  Contact *temp_contacts =
      (Contact *) realloc(all_contacts, sizeof(all_contacts) +
                                        sizeof(Contact));
  if (temp_contacts == NULL)
  {
    freeAll(all_contacts);
    printf("%s", ERR_MSG_OOM);
    exit(ERR_CODE_OOM);
  } else
  {
    all_contacts = temp_contacts;
    ++length;
  }

  printf("\nDEBUG: Allocated new mem!\n");


  printf("\nDEBUG: length of all_contacts after realloc: %d", length);

  all_contacts[length-1] = enterContact();
  //Next and previous
  all_contacts[length-2].next_ = &all_contacts[length-1];
  all_contacts[length-1].prev_ = &all_contacts[length-2];

  printf("NAME: %s", all_contacts[length].name_);
}

/**
 * evaluates length of all_contacts
 * @param all_contacts the array to evaluate by reference
 * @return length of all_contacts
 */
int getLengthOfAllContacts(Contact *all_contacts)
{
  int index = 0;
  if (all_contacts[0].next_ == NULL)
  {
    return 1;
  } else
  {
    Contact contact = all_contacts[index];
    while (contact.next_ != NULL)
    {
      contact = *contact.next_;
      ++index;
    }

    printf("DEBUG: Length: %d", index);
    return index;
  }
}

/**-----------------------------------------------------------------------------
 * enterContact
 * creates new instance of the Contact struct
 * data is entered by user
 * strings are created from heap mem for optimal performance
 *
 * @return
 */
Contact enterContact()
{
  Contact contact;

  contact.id_ = 0; //first contact gets first identifier

  //get Name
  printf("\nName: ");
  char *input_name = getInput();
  contact.name_ = (char *) malloc(strlen(input_name) * sizeof(char));
  if (contact.name_ == NULL)
  {
    free(input_name);
    printf("%s", ERR_MSG_OOM);
    exit(ERR_CODE_OOM);
  } else
  {
    strcpy(contact.name_, input_name);
  }

//  get Email Adress
  printf("\nEmail: ");
  char *input_mail = getInput();
  contact.email_address_ = (char *) malloc(strlen(input_mail) * sizeof(char));
  if (contact.email_address_ == NULL)
  {
    free(input_name);
    free(input_mail);
    printf("%s", ERR_MSG_OOM);
    exit(ERR_CODE_OOM);
  } else
  {
    strcpy(contact.email_address_, input_mail);
  }

  //  get Tel Number
  printf("\nTelephone: ");
  char *input_tel = getInput();
  contact.tel_number_ = (char *) malloc(strlen(input_tel) * sizeof(char));
  if (contact.tel_number_ == NULL)
  {
    free(input_tel);
    free(input_name);
    free(input_mail);
    printf("%s", ERR_MSG_OOM);
    exit(ERR_CODE_OOM);
  } else
  {
    strcpy(contact.tel_number_, input_tel);
  }

  free(input_tel);
  free(input_name);
  free(input_mail);

  //get Age
  printf("\nAge: ");
  scanf("%hu", &contact.age_);

  //return the newly created contact
  return contact;
}

/**-----------------------------------------------------------------------------
 * getInput()
 * reads user input from stdin dynamically
 * block size is BUFFER_SIZE (20)
 *
 * @author Stracke, 27.11.2016
 * @return the finished string read in from stdin
 */
char *getInput()
{
  int character;
  int length;
  length = 0;
  //initialise a buffer with a size of BUFFER_SIZE bytes
  char *buffer = (char *) malloc(BUFFER_SIZE * sizeof(char));

  //check for Nullpointer
  if (buffer == NULL)
  {

    printf("%s", ERR_MSG_OOM);
    exit(ERR_CODE_OOM);
  }

  while ((character = (char) getchar()) != EOF && character != '\n')
  {
    buffer[length] = (char) character;

    if ((length % (BUFFER_SIZE - 1) == 0) && length != 0)
    {
      printf("reallocing!\n");
      char *temp_buffer = (char *) realloc(buffer, length + (BUFFER_SIZE *
                                                             sizeof(char)));
      if (temp_buffer == _NULL)
      {
        free(buffer);
        printf("%s", ERR_MSG_OOM);
        exit(ERR_CODE_OOM);
      } else
      {
        buffer = temp_buffer;
      }
    }

    ++length;
  }

  buffer[length] = '\0';

  return buffer;
}

/**-----------------------------------------------------------------------------
 *  printStartScreen()
 *  just prints a small start screen for optical purposes
 *  @author Stracke, 27.11.2016
 */
void printStartScreen()
{
  printf("--------------Contacts------------\n");
  printf("-                                -\n");
  printf("-                                -\n");
  printf("-             Welcome            -\n");
  printf("- Please enter the first contact -\n");
}

/**-----------------------------------------------------------------------------
 * printMenu
 * prints Menu for interaction with application
 *
 */
void printMenu()
{
  printf("--------------Contacts------------\n");
  printf("                Menu              \n");
  printf("1) Show all contacts              \n");
  printf("2) enter new contact              \n");
  printf("3) exit                           \n");
  printf("\nAction: ");
}

void freeAll(Contact* all_contacts)
{
  Contact *contact = &all_contacts[0];
  printf("\nDEBUG: freeAll: first contact id: %d", contact->id_);
  if (contact->id_ >= 0)
  {
    do
    {
      free(contact->name_);
      free(contact->email_address_);
      free(contact->tel_number_);

      printf("\nDEBUG: Successfully freed all contact properties");

      contact = contact->next_;
    }
    while(contact != NULL);

    printf("\nDEBUG: Successfully freed all_contacts!");
    free(all_contacts);
  }
  else
  {
    free(all_contacts);
  }
}